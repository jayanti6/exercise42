function TabsCreator(domElements) {
    this.divModules = domElements.divModules;
}

TabsCreator.prototype.hideAllModules = function() {
  this.divModules.hide();
};

TabsCreator.prototype.createList = function() {
  this.unorderedList = $("<ul></ul>");
  var firstDivModule = this.divModules.first();
  firstDivModule.before(this.unorderedList);
};

TabsCreator.prototype.addTextToList = function() {
  var _this = this;
  this.divModules.each(function(){
    var thisDivModule = $(this);
    var listItem = $("<li></li>");
    listItem.text(thisDivModule.children("h2").text());
    _this.unorderedList.append(listItem);
    listItem.data("divModule", thisDivModule);
    _this.bindClickToListItems(listItem);
  });
};

TabsCreator.prototype.bindClickToListItems = function(listItem) {
  var _this = this;
  listItem.on("click", function(){
    var thisListItem = $(this);
    var module = thisListItem.data("divModule").show();
    _this.divModules.not(module).hide();
    thisListItem.addClass("current").siblings().removeClass("current");
  });
};

TabsCreator.prototype.showFirstTab = function() {
  this.divModules.first().show();
};

TabsCreator.prototype.init = function() {
  this.hideAllModules();
  this.createList();
  this.addTextToList();
  this.showFirstTab();
};

$(document).ready(function() {
  var domElements = {
    divModules: $("div.module")
  };
  var tabsCreator = new TabsCreator(domElements);
  tabsCreator.init();
});